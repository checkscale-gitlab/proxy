# Proxy
Nginx Proxy Container with Let's Encrypt Support.

## Usage
Install [Docker](https://docs.docker.com/engine/installation/) & [Docker Compose](https://docs.docker.com/compose/) and make sure you're able to run `hello-world`:

```bash
docker run hello-world
```

Clone this repo and enter the directory:

```bash
git clone https://gitlab.com/wp-id/docker/proxy.git
cd proxy
```

Create external network:

```bash
docker network create proxy
```

Start the containers:

```bash
docker-compose up -d
```

Start any containers you want proxied with an env var `VIRTUAL_HOST=mydomain.com`:

```bash
docker run -e VIRTUAL_HOST=mydomain.com  ...
```

For automatic Let's Encrypt SSL support:

```bash
docker run \
  -e VIRTUAL_HOST=mydomain.com \
  -e LETSENCRYPT_HOST=mydomain.com \
  -e LETSENCRYPT_EMAIL=me@mydomain.com \
  ...
```

To stop the proxy containers, run:

```bash
docker-compose down -v
```

More info:
* https://github.com/jwilder/nginx-proxy
* https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion

